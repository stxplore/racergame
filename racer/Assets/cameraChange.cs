﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraChange : MonoBehaviour {
    public Camera[] cameras;
    public int curr_cam = 0;

	// Use this for initialization
	void Start () {
  
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.C))
        {
            cameras[curr_cam].enabled = false;
            curr_cam = (++curr_cam) % cameras.Length;
            cameras[curr_cam].enabled = true;
        }
	}
}
