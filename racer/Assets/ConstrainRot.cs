﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstrainRot : MonoBehaviour {

    public float MaxX, MaxZ;
    public GameObject parent;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        parent.transform.localEulerAngles = new Vector3(Mathf.Clamp(parent.transform.localEulerAngles.x, -1*MaxX, MaxX), parent.transform.localEulerAngles.y, Mathf.Clamp(parent.transform.localEulerAngles.z, -1 * MaxZ, MaxZ));
    }
}
